import 'package:flutter/material.dart';
import 'package:todo_list/models/to_do.dart';

class DisplayToDo extends StatelessWidget {
  DisplayToDo({Key key, this.todo}) : super(key: key);

  final ToDo todo;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {
          todo.changeCheck();
          Navigator.of(context).pop(todo.checked);
        },
        tooltip: 'Check it or not',
        label: !todo.checked ? Text('Check') : Text('Uncheck'),
        icon: !todo.checked ? Icon(Icons.check) : Icon(Icons.clear),
      ),
      appBar: AppBar(
        title: Text(todo.title),
      ),
      body: Container(
        padding: const EdgeInsets.all(12.0),
        child: Center(
          child: Column(
            children: <Widget>[
              Text(
                todo.subtitle,
                style: TextStyle(
                    fontSize: 24,
                    fontStyle: FontStyle.italic,
                    fontWeight: FontWeight.bold),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
