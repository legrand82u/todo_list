import 'package:flutter/material.dart';
import 'package:todo_list/models/to_do.dart';
import 'package:todo_list/widgets/my_form.dart';

import 'display_to_do.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key key}) : super(key: key);

  static List<ToDo> todos = [
    ToDo(title: 'Premier', subtitle: 'This is a subtitle', checked: false),
    ToDo(title: 'Second', subtitle: 'This is a subtitle', checked: false),
    ToDo(title: 'Third', subtitle: 'This is a subtitle', checked: false),
    ToDo(title: 'fourth', subtitle: 'This is a subtitle', checked: false),
    ToDo(title: 'fourth', subtitle: 'This is a subtitle', checked: false),
    ToDo(title: 'fourth', subtitle: 'This is a subtitle', checked: false),
  ];

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {
          _handleAdd(context);
        },
      ),
      backgroundColor: Colors.grey.shade300,
      appBar: AppBar(
        title: Text('To-Do List'),
      ),
      body: listToDo(),
    );
  }

  Widget listToDo() {
    return Container(
      child: Center(
        child: ListView.separated(
          itemCount: HomePage.todos.length,
          separatorBuilder: (context, index) {
            return Divider();
          },
          itemBuilder: (context, int index) {
            return ListTile(
              leading: HomePage.todos[index].checked
                  ? Icon(Icons.check_box)
                  : Icon(Icons.check_box_outline_blank),
              title: Text(HomePage.todos[index].title),
              subtitle: Text(HomePage.todos[index].subtitle),
              onTap: () {
                _handleDetails(context, index);
              },
            );
          },
        ),
      ),
    );
  }

  void _handleDetails(BuildContext context, int index) async {
    bool result = await Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) {
          return DisplayToDo(todo: HomePage.todos[index]);
        },
      ),
    );
    if (result != null) {
      setState(() {
        HomePage.todos[index].checked = result;
      });
    }
  }

  void _handleAdd(BuildContext context) async {
    ToDo result = await showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          content: MyForm(),
        );
      },
    );

    if (result != null) {
      setState(() {
        HomePage.todos.add(result);
      });
    }
  }
}
