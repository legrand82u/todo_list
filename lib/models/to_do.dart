class ToDo {
  final String title;
  final String subtitle;
  bool checked;

  ToDo({this.title, this.subtitle, this.checked});

  void changeCheck() {
    checked = !checked;
  }
}
