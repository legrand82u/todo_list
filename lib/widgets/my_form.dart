import 'package:flutter/material.dart';
import 'package:todo_list/models/to_do.dart';

class MyForm extends StatefulWidget {
  MyForm({Key key}) : super(key: key);

  @override
  _MyFormState createState() => _MyFormState();
}

class _MyFormState extends State<MyForm> {
  final _formKey = GlobalKey<FormState>();
  final myController = TextEditingController();
  final myController2 = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Form(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          TextFormField(
            controller: myController,
            validator: (value) {
              if (value.isEmpty) {
                return 'Enter a title';
              }
              return null;
            },
            decoration: InputDecoration(
              labelText: 'Title',
              icon: Icon(Icons.title),
              labelStyle: TextStyle(fontSize: 20),
            ),
          ),
          TextFormField(
            controller: myController2,
            validator: (value) {
              if (value.isEmpty) {
                return 'Enter a subtitle';
              }
              return null;
            },
            decoration: InputDecoration(
              labelText: 'Subtitle',
              icon: Icon(Icons.subtitles),
              labelStyle: TextStyle(fontSize: 20),
            ),
          ),
          RaisedButton(
            disabledColor: Colors.grey,
            color: Colors.blue,
            onPressed: () {
              FocusScopeNode currentFocus = FocusScope.of(context);
              if (!currentFocus.hasPrimaryFocus) {
                currentFocus.unfocus();
              }

              if (_formKey.currentState.validate()) {
                print(myController.text);
                print(myController2.text);
                ToDo todo = ToDo(
                  title: myController.text,
                  subtitle: myController2.text,
                  checked: false,
                );

                Navigator.of(context).pop(todo);
              }
            },
            child: Text('Submit'),
          )
        ],
      ),
      key: _formKey,
    );
  }
}
